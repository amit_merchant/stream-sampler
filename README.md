Stream Sampler
===

> This is a simple stream sampler that receives	and	processes an input stream consisting of single characters.

## Install

```bash
$ cd stream-sampler
$ composer install
```

## Usage

Run following usecases in order to check the output sample

```bash
$ php .\index -i THEQUICKBROWNFOXJUMPSOVERTHELAZYDOG

$ php .\index -i "http://www.random.org/strings/?num=100&len=8&digits=on&upperalpha=on&loweralpha=on&unique=on&format=plain&rnd=new"

$ dd if=/dev/urandom count=100 bs=1MB | base64 | php .\index -p 5
```

## License

MIT